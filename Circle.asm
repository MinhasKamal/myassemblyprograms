##Writer: Minhas Kamal
##Date: 02-MAY-2014
##Function:	Calculates circumferance & area of a circle.

#####**data**#####
.data

prompt: .asciiz "Enter the diameter of the circle: "
PI: 	.double 3.1416
four: 	.double 4.0
circumference: .asciiz "\nThe circumferance is: "
area: 	.asciiz "\nThe area is: "



#####**text**#####
.text

main:
	la $a0, prompt			#prompt for user input
	li $v0, 4
	syscall
	
	li $v0, 7				#take input of a floating number
	syscall
	
	l.d $f2, PI				#load PI
	
	
	mul.d $f4, $f2, $f0		#circumferance = PI * diameter
	
	la $a0, circumference	#print result 
	li $v0, 4
	syscall
	mov.d $f12, $f4			
	li $v0, 3
	syscall
	
	
	mul.d $f6, $f2, $f0		#area = PI * diameter * diameter / 4
	mul.d $f6, $f6, $f0
	l.d $f8, four
	div.d $f6, $f6, $f8
	
	la $a0, area			#print result
	li $v0, 4
	syscall
	mov.d $f12, $f6			
	li $v0, 3
	syscall

exit:
	li $v0, 10
	syscall
