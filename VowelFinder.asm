##Writer: Minhas Kamal
##Date: 25-FEB-2014
##Function: Takes a char input and prints if it is a vowel or constant


#####data#####
.data

prompt: .asciiz "Enter your character: "
msg1: 	.asciiz "  It is a vowel.\n"
msg2: 	.asciiz "  It is a consonant.\n"
msg3: 	.asciiz "  Not a character.\n"


#####text#####
.text

main:

	la $a0, prompt		#ask for user input
	li $v0, 4
	syscall

	li $v0, 12			#take input
	syscall
	move $t1, $v0

	##vowels
	beq $t1, 'a', vowel
	beq $t1, 'e', vowel
	beq $t1, 'i', vowel
	beq $t1, 'o', vowel
	beq $t1, 'u', vowel
	
	beq $t1, 'A', vowel
	beq $t1, 'E', vowel
	beq $t1, 'I', vowel
	beq $t1, 'O', vowel
	beq $t1, 'U', vowel
	
	##not character
	ble $t1, 'A', notChar
	bgt $t1, 'z', notChar
	
	##consonants
	j consonant

vowel:
	la $a0, msg1
	j printAndExit

consonant:
	la $a0, msg2
	j printAndExit

notChar:
	la $a0, msg3
	j printAndExit

printAndExit:
	li $v0, 4
	syscall
	li $v0, 10
	syscall
