##Writer: Minhas Kamal
##Date: 03-March-2014
##Function:	Finds the highest and lowest integer from a definite number of integers


#####**data**#####
.data

prompt1: 		.asciiz "\n\nEnter number of integers: "
prompt2: 		.asciiz "\nEnter your integers: "
msg_highest: 	.asciiz "\nHighest number is: "
msg_lowest: 	.asciiz "  Lowest number is: "

#####**text**#####
.text

main:
	la $a0, prompt1
	li $v0, 4
	syscall
	
	li $v0, 5	#read number of integers
	syscall
	addi $t1, $v0, -1	#$t1=$v0-1
	

	la $a0, prompt2
	li $v0, 4
	syscall
	
	li $v0, 5	#read first integer
	syscall
	
	add $t9, $v0, $zero	#initialization #contains highest 
	add $t8, $v0, $zero	#initialization #contains lowest
	add $t0, $zero, $zero	#initialization #loop counter
	
loop:
	li $v0, 5	#read integer
	syscall
	
	blt $v0, $t9, next1	#if($v0<$t9) do nothing
	add $t9, $v0, $zero

	next1:
	
	blt $t8, $v0, next2	#if($t8<$v0) do nothing
	add $t8, $v0, $zero

	next2:
	
	add $t0, $t0, 1		#$t0++
	blt $t0, $t1, loop	#repeats $t1 times


highest:
	la $a0, msg_highest
	li $v0, 4
	syscall
	
	add $a0, $t9, $zero	#loading highest value
	li $v0, 1
	syscall
	
lowest:
	la $a0, msg_lowest
	li $v0, 4
	syscall
	
	add $a0, $t8, $zero	#loading lowest value
	li $v0, 1
	syscall
	
exit:
	li $v0, 10
	syscall